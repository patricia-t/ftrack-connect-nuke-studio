..
    :copyright: Copyright (c) 2014 ftrack

**********
Developing
**********

Discover how to develop and extend the default ftrack integration with Nuke
studio.

.. toctree::
    :maxdepth: 1

    processors
    event_list
    adding_custom_templates
    api_reference/index
