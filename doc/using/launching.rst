..
    :copyright: Copyright (c) 2015 ftrack

.. _using/launching:

*********
Launching
*********

The primary way of launching Nuke studio with the ftrack integration is to use
download and use the
:ref:`ftrack Connect package <using/installing_ftrack_connect_package>`.

Once installed and running you can start Nuke studio from the Actions tab:

.. image:: /image/ftrack_connect_actions.png
